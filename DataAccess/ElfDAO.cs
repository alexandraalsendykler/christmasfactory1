﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using MySql.Data.MySqlClient;
using System.Data.Common;

using Fr.EQL.AI110.ChristmasFactory.Entities;

namespace Fr.EQL.AI110.ChristmasFactory.DataAccess
{
    // en java on utilise "extends"
    // en C# on utilise ":"
    public class ElfDAO : DAO
    {
        public void Insert(Elf elf)
        {
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection :
            cnx.ConnectionString = CNX_STR;

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"INSERT INTO elf 
                                (name, is_available, picture) 
                                VALUES 
                                (@name, @isAvailable, @picture)";

            cmd.Parameters.Add(new MySqlParameter("name", elf.Name));
            cmd.Parameters.Add(new MySqlParameter("isAvailable", elf.IsAvailable));
            cmd.Parameters.Add(new MySqlParameter("picture", elf.Picture));

            // 3 - ouvrir la connection :
            cnx.Open();

            // 4 - Exécuter la commande :
            int nbLignes = cmd.ExecuteNonQuery();

            // 5 - Traiter le résultat :
            Console.WriteLine(nbLignes + " ligne insérée");

            // 6 - fermer la conneciton :
            cnx.Close();
        }

        public void Update(Elf elf)
        {
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection :
            cnx.ConnectionString = CNX_STR;

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"UPDATE 
                                    elf 
                                SET name = @name,
                                    is_available = @isAvailable,
                                    picture = @picture
                                WHERE 
                                    id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", elf.Id));
            cmd.Parameters.Add(new MySqlParameter("name", elf.Name));
            cmd.Parameters.Add(new MySqlParameter("isAvailable", elf.IsAvailable));
            cmd.Parameters.Add(new MySqlParameter("picture", elf.Picture));

            // 3 - ouvrir la connection :
            cnx.Open();

            // 4 - Exécuter la commande :
            int nbLignes = cmd.ExecuteNonQuery();

            // 5 - Traiter le résultat :
            Console.WriteLine(nbLignes + " ligne mise à jour");

            // 6 - fermer la connection :
            cnx.Close();
        }

        public void Delete(int id)
        {
            // créer un objet connection :
            DbConnection cnx = new MySqlConnection();

            // 1 - Paramétrer la connection :
            cnx.ConnectionString = CNX_STR;

            // 2 - Paramétrer la commande :
            DbCommand cmd = cnx.CreateCommand();
            cmd.CommandText = @"DELETE FROM elf 
                                WHERE id = @id";

            cmd.Parameters.Add(new MySqlParameter("id", id));

            // 3 - ouvrir la connection :
            cnx.Open();

            // 4 - Exécuter la commande :
            int nbLignes = cmd.ExecuteNonQuery();

            // 5 - Traiter le résultat :
            Console.WriteLine(nbLignes + " ligne supprimée");

            // 6 - fermer la connection :
            cnx.Close();
        }

        public Elf GetById(int id)
        {
            return null;
        }

        public List<Elf> GetAll()
        {
            //             java vs    C#
            // interface : List       IList 
            // classe :    ArrayList  List

            return null;
        }
    }
}
